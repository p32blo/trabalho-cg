
#define _USE_MATH_DEFINES
#include <cmath>

#include <iostream>
#include <fstream>

#include <sstream>
#include <string>

#include <vector>


typedef struct sPonto {
	double x, y, z; // coordenadas
	double nX, nY, nZ; // normais
	double tX, tY; // normais
} Ponto;

typedef struct sTriangulo {
	Ponto a, b, c;
} Triangulo;


double lenght_v3(double *vec)
{
	return sqrt((vec[0] * vec[0]) + (vec[1] * vec[1]) + (vec[2] * vec[2]));
}

void normalize_v3(double *vec)
{
	double fac;
	if ((fac = lenght_v3(vec)) > 0.0001) {
		vec[0] /= fac;
		vec[1] /= fac;
		vec[2] /= fac;
	}
}

void construirCone(std::vector<Triangulo> *res, double raio, double altura, int fatias, int camadas)
{
	double delta = 2 * M_PI / fatias;
	double alpha = 0;

	double r = raio;
	double r_delta = raio / camadas;

	double h = -altura / 2;
	double h_delta = altura / camadas;

	for (int i = 0; i < fatias; i++) {
		Ponto p0, p1, p2;

		p0.x = 0;
		p0.y = h;
		p0.z = 0;
		p0.tX = p0.x;
		p0.tY = p0.z;

		p1.x = r * sin(alpha + delta);
		p1.y = h;
		p1.z = r * cos(alpha + delta);
		p1.tX = p1.x;
		p1.tY = p1.z;

		p2.x = r * sin(alpha);
		p2.y = h;
		p2.z = r * cos(alpha);
		p2.tX = p2.x;
		p2.tY = p2.z;

		//normais
		p0.nX = p1.nX = p2.nX = 0;
		p0.nY = p1.nY = p2.nY = -1;
		p0.nZ = p1.nZ = p2.nZ = 0;

		Triangulo t1 = { p0, p1, p2 };
		res->push_back(t1);

		// Segmentos laterais
		for (int j = 0; j < camadas; j++) {

			Ponto p3, p4, p5, p6;

			p3.x = (r - r_delta) * sin(alpha);
			p3.y = h + h_delta;
			p3.z = (r - r_delta) * cos(alpha);
			p3.tX = p3.x;
			p3.tY = p3.y;

			p4.x = (r - r_delta) * sin(alpha + delta);
			p4.y = h + h_delta;
			p4.z = (r - r_delta) * cos(alpha + delta);
			p4.tX = p4.x;
			p4.tY = p4.y;

			p5.x = r * sin(alpha + delta);
			p5.y = h;
			p5.z = r * cos(alpha + delta);
			p5.tX = p5.x;
			p5.tY = p5.y;

			p6.x = r * sin(alpha);
			p6.y = h;
			p6.z = r * cos(alpha);
			p6.tX = p6.x;
			p6.tY = p6.y;

			//normais
			double vec[3] = { sin(alpha), raio / altura, cos(alpha) };
			double vec1[3] = { sin(alpha + delta), raio / altura, cos(alpha + delta) };

			normalize_v3(vec);
			p3.nX = p6.nX = vec[0];
			p3.nY = p6.nY = vec[1];
			p3.nZ = p6.nZ = vec[2];

			normalize_v3(vec1);
			p4.nX = p5.nX = vec1[0];
			p4.nY = p5.nY = vec1[1];
			p4.nZ = p5.nZ = vec1[2];

			Triangulo t2, t3;
			t2.a = p6;
			t2.b = p5;
			t2.c = p3;

			t3.a = p3;
			t3.b = p5;
			t3.c = p4;

			res->push_back(t2);
			// Nao desenhar triangulos em pontos co-lineares
			if (j <= camadas) {
				res->push_back(t3);
			}

			r -= r_delta;
			h += h_delta;
		}
		alpha += delta;
		r = raio;
		h = -altura / 2;
	}
}

void construirPlano(std::vector<Triangulo> *res, double comp, double larg, int fatias, int camadas)
{
	double passoX = comp / fatias;
	double passoZ = larg / camadas;

	for (int i = 0; i < fatias; i++) {
		for (int j = 0; j < camadas; j++) {
			Ponto p0, p1, p2, p3;

			p0.x = (i * passoX) - (comp / 2);
			p0.y = 0;
			p0.z = (j * passoZ) - (larg / 2);
			p0.tX = p0.x; // textura
			p0.tY = p0.z; // textura

			p1.x = ((i + 1) * passoX) - (comp / 2);
			p1.y = 0;
			p1.z = (j * passoZ) - (larg / 2);
			p1.tX = p1.x; // textura
			p1.tY = p1.z; // textura

			p2.x = ((i + 1) * passoX) - (comp / 2);
			p2.y = 0;
			p2.z = ((j + 1) * passoZ) - (larg / 2);
			p2.tX = p2.x; // textura
			p2.tY = p2.z; // textura

			p3.x = (i * passoX) - (comp / 2);
			p3.y = 0;
			p3.z = ((j + 1) * passoZ) - (larg / 2);
			p3.tX = p3.x; // textura
			p3.tY = p3.z; // textura

			//normais
			p0.nX = p1.nX = p2.nX = p3.nX = 0;
			p0.nY = p1.nY = p2.nY = p3.nY = 1;
			p0.nZ = p1.nZ = p2.nZ = p3.nZ = 0;

			Triangulo t0, t1;
			t0 = { p0, p2, p1 };
			t1 = { p3, p2, p0 };

			res->push_back(t0);
			res->push_back(t1);
		}
	}
}

void construirParalelepipedo(std::vector<Triangulo> *res, double comp, double larg, double altura, int divisao)
{
	double c_delta = comp / divisao;
	double l_delta = larg / divisao;
	double a_delta = altura / divisao;

	/* face superior */

	double c = -comp / 2;
	double l = -larg / 2;
	double a = altura / 2;

	for (int i = 0; i < divisao; i++) {
		for (int j = 0; j < divisao; j++) {
			Ponto p0, p1, p2, p3;

			p0.x = c;
			p0.y = a;
			p0.z = l;
			p0.tX = p0.x; // textura
			p0.tY = p0.z; // textura

			p1.x = c + c_delta;
			p1.y = a;
			p1.z = l;
			p1.tX = p1.x; // textura
			p1.tY = p1.z; // textura

			p2.x = c + c_delta;
			p2.y = a;
			p2.z = l + l_delta;
			p2.tX = p2.x; // textura
			p2.tY = p2.z; // textura

			p3.x = c;
			p3.y = a;
			p3.z = l + l_delta;
			p3.tX = p3.x; // textura
			p3.tY = p3.z; // textura


			//normais
			p0.nX = p1.nX = p2.nX = p3.nX = 0;
			p0.nY = p1.nY = p2.nY = p3.nY = 1;
			p0.nZ = p1.nZ = p2.nZ = p3.nZ = 0;

			Triangulo t1, t2;
			t1 = { p1, p0, p3 };
			t2 = { p1, p3, p2 };

			res->push_back(t1);
			res->push_back(t2);

			c += c_delta;
		}
		c = -comp / 2;
		l += l_delta;
	}

	/* Face Inferior */

	c = -comp / 2;
	l = -larg / 2;
	a = -altura / 2;

	for (int i = 0; i < divisao; i++) {
		for (int j = 0; j < divisao; j++) {
			Ponto p0, p1, p2, p3;

			p0.x = c;
			p0.y = a;
			p0.z = l;
			p0.tX = p0.x; // textura
			p0.tY = p0.z; // textura

			p1.x = c + c_delta;
			p1.y = a;
			p1.z = l;
			p1.tX = p1.x; // textura
			p1.tY = p1.z; // textura

			p2.x = c + c_delta;
			p2.y = a;
			p2.z = l + l_delta;
			p2.tX = p2.x; // textura
			p2.tY = p2.z; // textura

			p3.x = c;
			p3.y = a;
			p3.z = l + l_delta;
			p3.tX = p3.x; // textura
			p3.tY = p3.z; // textura

			//normais
			p0.nX = p1.nX = p2.nX = p3.nX = 0;
			p0.nY = p1.nY = p2.nY = p3.nY = -1;
			p0.nZ = p1.nZ = p2.nZ = p3.nZ = 0;

			Triangulo t1, t2;
			t1 = { p0, p1, p3 };
			t2 = { p3, p1, p2 };

			res->push_back(t1);
			res->push_back(t2);

			c += c_delta;
		}
		c = -comp / 2;
		l += l_delta;
	}

	/* Face Anterior */

	l = larg / 2;
	a = -altura / 2;

	for (int i = 0; i < divisao; i++) {
		for (int j = 0; j < divisao; j++) {
			Ponto p0, p1, p2, p3;

			p0.x = c;
			p0.y = a;
			p0.z = l;
			p0.tX = p0.x; // textura
			p0.tY = p0.y; // textura

			p1.x = c + c_delta;
			p1.y = a;
			p1.z = l;
			p1.tX = p1.x; // textura
			p1.tY = p1.y; // textura

			p2.x = c + c_delta;
			p2.y = a + a_delta;
			p2.z = l;
			p2.tX = p2.x; // textura
			p2.tY = p2.y; // textura

			p3.x = c;
			p3.y = a + a_delta;
			p3.z = l;
			p3.tX = p3.x; // textura
			p3.tY = p3.y; // textura

			//normais
			p0.nX = p1.nX = p2.nX = p3.nX = 0;
			p0.nY = p1.nY = p2.nY = p3.nY = 0;
			p0.nZ = p1.nZ = p2.nZ = p3.nZ = 1;

			Triangulo t1, t2;
			t1 = { p0, p1, p3 };
			t2 = { p3, p1, p2 };

			res->push_back(t1);
			res->push_back(t2);

			c += c_delta;
		}
		c = -comp / 2;
		a += a_delta;
	}

	/* Face Posterior*/

	c = -comp / 2;
	l = -larg / 2;
	a = -altura / 2;

	for (int i = 0; i < divisao; i++) {
		for (int j = 0; j < divisao; j++) {
			Ponto p0, p1, p2, p3;

			p0.x = c;
			p0.y = a;
			p0.z = l;
			p0.tX = p0.x; // textura
			p0.tY = p0.y; // textura

			p1.x = c + c_delta;
			p1.y = a;
			p1.z = l;
			p1.tX = p1.x; // textura
			p1.tY = p1.y; // textura

			p2.x = c + c_delta;
			p2.y = a + a_delta;
			p2.z = l;
			p2.tX = p2.x; // textura
			p2.tY = p2.y; // textura

			p3.x = c;
			p3.y = a + a_delta;
			p3.z = l;
			p3.tX = p3.x; // textura
			p3.tY = p3.y; // textura

			//normais
			p0.nX = p1.nX = p2.nX = p3.nX = 0;
			p0.nY = p1.nY = p2.nY = p3.nY = 0;
			p0.nZ = p1.nZ = p2.nZ = p3.nZ = -1;

			Triangulo t1, t2;
			t1 = { p1, p0, p3 };
			t2 = { p1, p3, p2 };

			res->push_back(t1);
			res->push_back(t2);

			c += c_delta;
		}
		c = -comp / 2;
		a += a_delta;
	}

	/* Face Lateral Esquerda*/

	c = -comp / 2;
	l = -larg / 2;
	a = -altura / 2;

	for (int i = 0; i < divisao; i++) {
		for (int j = 0; j < divisao; j++) {
			Ponto p0, p1, p2, p3;

			p0.x = c;
			p0.y = a;
			p0.z = l;
			p0.tX = p0.y; // textura
			p0.tY = p0.z; // textura

			p1.x = c;
			p1.y = a;
			p1.z = l + l_delta;
			p1.tX = p1.y; // textura
			p1.tY = p1.z; // textura

			p2.x = c;
			p2.y = a + a_delta;
			p2.z = l + l_delta;
			p2.tX = p2.y; // textura
			p2.tY = p2.z; // textura

			p3.x = c;
			p3.y = a + a_delta;
			p3.z = l;
			p3.tX = p3.y; // textura
			p3.tY = p3.z; // textura

			//normais
			p0.nX = p1.nX = p2.nX = p3.nX = -1;
			p0.nY = p1.nY = p2.nY = p3.nY = 0;
			p0.nZ = p1.nZ = p2.nZ = p3.nZ = 0;

			Triangulo t1, t2;
			t1 = { p0, p1, p3 };
			t2 = { p3, p1, p2 };

			res->push_back(t1);
			res->push_back(t2);

			l += l_delta;
		}
		l = -larg / 2;
		a += a_delta;
	}

	/* Face Lateral Direita */

	c = comp / 2;
	l = -larg / 2;
	a = -altura / 2;

	for (int i = 0; i < divisao; i++) {
		for (int j = 0; j < divisao; j++) {
			Ponto p0, p1, p2, p3;

			p0.x = c;
			p0.y = a;
			p0.z = l;
			p0.tX = p0.y; // textura
			p0.tY = p0.z; // textura

			p1.x = c;
			p1.y = a;
			p1.z = l + l_delta;
			p1.tX = p1.y; // textura
			p1.tY = p1.z; // textura

			p2.x = c;
			p2.y = a + a_delta;
			p2.z = l + l_delta;
			p2.tX = p2.y; // textura
			p2.tY = p2.z; // textura

			p3.x = c;
			p3.y = a + a_delta;
			p3.z = l;
			p3.tX = p3.y; // textura
			p3.tY = p3.z; // textura

			//normais
			p0.nX = p1.nX = p2.nX = p3.nX = 1;
			p0.nY = p1.nY = p2.nY = p3.nY = 0;
			p0.nZ = p1.nZ = p2.nZ = p3.nZ = 0;

			Triangulo t1, t2;
			t1 = { p1, p0, p3 };
			t2 = { p1, p3, p2 };
			res->push_back(t1);
			res->push_back(t2);

			l += l_delta;
		}
		l = -larg / 2;
		a += a_delta;
	}
}

void construirEsfera(std::vector<Triangulo> *res, double raio, int fatias, int camadas)
{
	double passoAlfa = 2 * M_PI / fatias;
	double passoBeta = M_PI / camadas;

	double alfa = 0.0;
	double beta = -M_PI_2;


	for (int i = 0; i < camadas; i++) {
		for (int j = 0; j < fatias; j++) {
			double t1 = -i*1.0 / fatias;
			double t2 = -(i+1.0) / fatias;
			double s1 = j*1.0 / camadas;
			double s2 = (j+1.0) / camadas;


			Ponto p0, p1, p2, p3;
			p0.x = raio * cos(beta) * sin(alfa);
			p0.y = raio * sin(beta);
			p0.z = raio * cos(beta) * cos(alfa);
			//normal
			p0.nX = cos(beta) * sin(alfa);
			p0.nY = sin(beta);
			p0.nZ = cos(beta) * cos(alfa);
			p0.tX = s1;
			p0.tY = t1;

			p1.x = raio * cos(beta + passoBeta) * sin(alfa);
			p1.y = raio * sin(beta + passoBeta);
			p1.z = raio * cos(beta + passoBeta) * cos(alfa);
			//normal
			p1.nX = cos(beta + passoBeta) * sin(alfa);
			p1.nY = sin(beta + passoBeta);
			p1.nZ = cos(beta + passoBeta) * cos(alfa);
			p1.tX = s1;
			p1.tY = t2; // textura

			p2.x = raio * cos(beta + passoBeta) * sin(alfa + passoAlfa);
			p2.y = raio * sin(beta + passoBeta);
			p2.z = raio * cos(beta + passoBeta) * cos(alfa + passoAlfa);
			//normal
			p2.nX = cos(beta + passoBeta) * sin(alfa + passoAlfa);
			p2.nY = sin(beta + passoBeta);
			p2.nZ = cos(beta + passoBeta) * cos(alfa + passoAlfa);
			p2.tX = s2; // textura
			p2.tY = t2; // textura

			p3.x = raio * cos(beta) * sin(alfa + passoAlfa);
			p3.y = raio * sin(beta);
			p3.z = raio * cos(beta) * cos(alfa + passoAlfa);
			//normal
			p3.nX = cos(beta) * sin(alfa + passoAlfa);
			p3.nY = sin(beta);
			p3.nZ = cos(beta) * cos(alfa + passoAlfa);
			p3.tX = s2; // textura
			p3.tY = t1; // textura

			Triangulo tr0, tr1;
			tr0 = { p2, p1, p0 };
			tr1 = { p2, p0, p3 };

			// Nao desenhar triangulos em pontos co-lineares
			if (i < camadas){
				res->push_back(tr0);
			}
			if (i > 0){
				res->push_back(tr1);
			}

			alfa += passoAlfa;
		}
		beta += passoBeta;
	}
}

Ponto pontoDeBezier(float u, float v, Ponto pC[]) {

	int m[4][4] = {
		{ -1, 3, -3, 1 },
		{ 3, -6, 3, 0 },
		{ -3, 3, 0, 0 },
		{ 1, 0, 0, 0 }
	};

	float us[4] = { 1, u, pow(u, 2), pow(u, 3) };
	float vs[4] = { 1, v, pow(v, 2), pow(v, 3) };

	//U*M*G*M^T*V
	float UM[4];
	for (int i = 0; i < 4; i++) {
		UM[i] = us[3] * m[0][i]
			+ us[2] * m[1][i]
			+ us[1] * m[2][i]
			+ us[0] * m[3][i];
	}

	float MTV[4];
	for (int i = 0; i < 4; i++) {
		MTV[i] = m[i][0] * vs[3]
			+ m[i][1] * vs[2]
			+ m[i][2] * vs[1]
			+ m[i][3] * vs[0];
	}

	double matPC[4][4][3] = {
		{ { pC[0].x, pC[0].y, pC[0].z }, { pC[1].x, pC[1].y, pC[1].z }, { pC[2].x, pC[2].y, pC[2].z }, { pC[3].x, pC[3].y, pC[3].z } },
		{ { pC[4].x, pC[4].y, pC[4].z }, { pC[5].x, pC[5].y, pC[5].z }, { pC[6].x, pC[6].y, pC[6].z }, { pC[7].x, pC[7].y, pC[7].z } },
		{ { pC[8].x, pC[8].y, pC[8].z }, { pC[9].x, pC[9].y, pC[9].z }, { pC[10].x, pC[10].y, pC[10].z }, { pC[11].x, pC[11].y, pC[11].z } },
		{ { pC[12].x, pC[12].y, pC[12].z }, { pC[13].x, pC[13].y, pC[13].z }, { pC[14].x, pC[14].y, pC[14].z }, { pC[15].x, pC[15].y, pC[15].z } },
	};

	double coords[3];
	float usCoef[4] = { 0, 1, 2 * u, 3 * pow(u, 2) };
	float vsCoef[4] = { 0, 1, 2 * v, 3 * pow(v, 2) };
	float UcoefM[4];
	for (int i = 0; i < 4; i++) {
		UcoefM[i] = usCoef[3] * m[0][i]
			+ usCoef[2] * m[1][i]
			+ usCoef[1] * m[2][i]
			+ usCoef[0] * m[3][i];
	}
	float MTVcoef[4];
	for (int i = 0; i < 4; i++) {
		MTVcoef[i] = m[i][0] * vsCoef[3]
			+ m[i][1] * vsCoef[2]
			+ m[i][2] * vsCoef[1]
			+ m[i][3] * vsCoef[0];
	}


	double tangU[3];
	double tangV[3];
	for (int c = 0; c < 3; c++) {
		double UMG[4];
		double UcoefMG[4];
		for (int i = 0; i < 4; i++) {
			UMG[i] = UM[0] * matPC[0][i][c]
				+ UM[1] * matPC[1][i][c]
				+ UM[2] * matPC[2][i][c]
				+ UM[3] * matPC[3][i][c];

			UcoefMG[i] = UcoefM[0] * matPC[0][i][c]
				+ UcoefM[1] * matPC[1][i][c]
				+ UcoefM[2] * matPC[2][i][c]
				+ UcoefM[3] * matPC[3][i][c];
		}

		coords[c] = UMG[0] * MTV[0]
			+ UMG[1] * MTV[1]
			+ UMG[2] * MTV[2]
			+ UMG[3] * MTV[3];

		tangU[c] = UcoefMG[0] * MTV[0]
			+ UcoefMG[1] * MTV[1]
			+ UcoefMG[2] * MTV[2]
			+ UcoefMG[3] * MTV[3];

		tangV[c] = UMG[0] * MTVcoef[0]
			+ UMG[1] * MTVcoef[1]
			+ UMG[2] * MTVcoef[2]
			+ UMG[3] * MTVcoef[3];
	}

	Ponto res;
	res.x = coords[0];
	res.y = coords[1];
	res.z = coords[2];

	double normal[3];
	normal[0] = (tangV[1] * tangU[2]) - (tangV[2] * tangU[1]);
	normal[1] = (tangV[2] * tangU[0]) - (tangV[0] * tangU[2]);
	normal[2] = (tangV[0] * tangU[1]) - (tangV[1] * tangU[0]);
	normalize_v3(normal);
	res.nX = normal[0];
	res.nY = normal[1];
	res.nZ = normal[2];

	res.tX = u; // textura -> VER!
	res.tY = v; // textura -> VER!

	//	if (!lenght_v3(tangV) || !lenght_v3(tangU)) printf("%f %f %f\n", res.x, res.y, res.z);

	return res;
}

void construirPatch(std::vector<Triangulo> *res, int fatias, int camadas, int indices[], Ponto pontos[])
{
	Ponto pontosControlo[16];
	for (int i = 0; i < 16; i++) {
		pontosControlo[i] = pontos[indices[i]];
	}

	for (int i = 0; i < fatias; i++) {
		float v = i*1.0f / fatias;
		float v1 = (i + 1)*1.0f / fatias;

		for (int j = 0; j < camadas; j++) {
			float u = j*1.0f / camadas;
			float u1 = (j + 1)*1.0f / camadas;

			Ponto a, b, c, d;
			a = pontoDeBezier(u, v, pontosControlo);
			b = pontoDeBezier(u, v1, pontosControlo);
			c = pontoDeBezier(u1, v, pontosControlo);
			d = pontoDeBezier(u1, v1, pontosControlo);

			Triangulo t1, t2;
			t1 = { a, b, c };
			t2 = { c, b, d };

			res->push_back(t1);
			res->push_back(t2);
		}
	}
}

void construirBezier(std::vector<Triangulo> *res, std::string srcBezier, int fatias, int camadas)
{
	std::ifstream file;

	file.open(srcBezier);
	if (!file) {
		return;
	}

	std::string linha;
	std::getline(file, linha);
	int nPatches = atoi(linha.c_str());

	std::vector<int*> patches;
	for (int i = 0; i < nPatches && std::getline(file, linha); i++) {
		int* indicePatch = (int*)malloc(sizeof(int)* 16);
		std::istringstream ss(linha);
		std::string num;
		for (int j = 0; j < 16 && std::getline(ss, num, ','); j++) {
			indicePatch[j] = atoi(num.c_str());
		}
		patches.push_back(indicePatch);
	}

	std::getline(file, linha);
	int nPontos = atoi(linha.c_str());
	Ponto* pontos = (Ponto*)malloc(sizeof(Ponto)* nPontos);

	for (int i = 0; i < nPontos && std::getline(file, linha); i++) {
		Ponto p;

		std::istringstream ss(linha);
		std::string num;

		std::getline(ss, num, ',');
		p.x = atof(num.c_str());

		std::getline(ss, num, ',');
		p.y = atof(num.c_str());

		std::getline(ss, num, ',');
		p.z = atof(num.c_str());

		pontos[i] = p;
	}

	for (int i = 0; i < nPatches; i++) {
		construirPatch(res, fatias, camadas, patches[i], pontos);
	}
}

int saveListaTriangulos(std::vector<Triangulo>* lista, std::string srcDest)
{
	std::fstream file;
	file.open(srcDest, std::fstream::out);
	if (!file) {
		return EXIT_FAILURE;
	}

	file << lista->size(); // indica quantos triangulos (linhas) est�o representados no ficheiro

	for (Triangulo t : *lista) {
		file << "\n";
		file << t.a.x << " " << t.a.y << " " << t.a.z << " ";
		file << t.b.x << " " << t.b.y << " " << t.b.z << " ";
		file << t.c.x << " " << t.c.y << " " << t.c.z << " ";

		file << t.a.nX << " " << t.a.nY << " " << t.a.nZ << " ";
		file << t.b.nX << " " << t.b.nY << " " << t.b.nZ << " ";
		file << t.c.nX << " " << t.c.nY << " " << t.c.nZ << " ";

		file << t.a.tX << " " << t.a.tY << " ";
		file << t.b.tX << " " << t.b.tY << " ";
		file << t.c.tX << " " << t.c.tY;
	}

	file.close();

	return EXIT_SUCCESS;
}

int main(int argc, char** argv)
{
	if (argc < 2) {
		std::cout << "Utiliza��o: [objecto] [caracteristicas ...] [resultado]" << std::endl;
		return EXIT_FAILURE;
	}

	std::vector<Triangulo> res;

	std::string arg(argv[1]);
	if (arg == "plano") {
		if (argc < 3 + 3) {
			std::cout << "Utiliza��o: plano [comprimento] [largura] [resolu��o] [resultado]" << std::endl;
			return EXIT_FAILURE;
		}

		double comp = atof(argv[2]);
		double larg = atof(argv[3]);
		int resolucao = atoi(argv[4]);

		construirPlano(&res, comp, larg, resolucao, resolucao);
	}
	else if (arg == "paralelepipedo") {
		if (argc < 3 + 4) {
			std::cout << "Utiliza��o: paralelepipedo [comprimento] [largura] [altura] [resolucao] [resultado]" << std::endl;
			return -1;
		}

		double comp = atof(argv[2]);
		double larg = atof(argv[3]);
		double altura = atof(argv[4]);
		int resolucao = atoi(argv[5]);

		construirParalelepipedo(&res, comp, larg, altura, resolucao);
	}
	else if (arg == "esfera") {
		if (argc < 3 + 3) {
			std::cout << "Utiliza��o: esfera [raio] [fatias] [camadas] [resultado]" << std::endl;
			return -1;
		}

		double raio = atof(argv[2]);
		int fatias = atoi(argv[3]);
		int camadas = atoi(argv[4]);

		construirEsfera(&res, raio, fatias, camadas);
	}
	else if (arg == "cone") {
		if (argc < 3 + 4) {
			std::cout << "Utiliza��o: cone [raio] [altura] [fatias] [camadas] [resultado]" << std::endl;
			return -1;
		}

		double raio = atof(argv[2]);
		double altura = atof(argv[3]);

		int fatias = atoi(argv[4]);
		int camadas = atoi(argv[5]);

		construirCone(&res, raio, altura, fatias, camadas);
	}
	else if (arg == "bezier") {
		if (argc < 3 + 3) {
			std::cout << "Utiliza��o: bezier [srcBezier] [fatias] [camadas] [resultado]" << std::endl;
			return -1;
		}

		std::string srcBezier(argv[2]);
		int fatias = atoi(argv[3]);
		int camadas = atoi(argv[4]);

		construirBezier(&res, srcBezier, fatias, camadas);
	}
	else {
		std::cout << "[objecto] :: plano | paralelepipedo | esfera | cone | bezier" << std::endl;
		return -2;
	}

	return saveListaTriangulos(&res, argv[argc - 1]);
}
