
#define _USE_MATH_DEFINES
#include <cmath>

#include <iostream>
#include <fstream>

#include <unordered_map>
#include <vector>
#include <array>
#include <string>

#include <glew.h>
#include <IL/il.h>

#define GLUT_DISABLE_ATEXIT_HACK
#include <GL/glut.h>

#include "tinyxml/tinyxml.h"


/* Typedef's */

typedef struct sPonto {
	double x, y, z;
} Ponto;

typedef struct sTriangulo {
	Ponto a, b, c;
} Triangulo;

typedef struct sModelo {
	GLuint buff[3];
	unsigned int numTris;
} Modelo;

typedef struct sTextura {
	GLuint texID;
} Textura;

typedef std::unordered_map<std::string, Modelo> BancoModelos;
typedef std::unordered_map<std::string, Textura> BancoTexturas;

typedef enum eLIGHTS {POINT, DIRECT, SPOT, ERR} Lights;

/* Variaveis Globais */

TiXmlDocument cena;
BancoModelos modelos;
BancoTexturas texturas;

std::vector<double> buffer_coords;
std::vector<double> buffer_norms;

float olharX, olharY, normFac;
bool toogle_path, toogle_normals;

float camX, camY, camZ;
int startX, startY, tracking = 0;

int camAlpha = 0, camBeta = 0, camRaio = 5;

/* Prototipo de funcoes */

std::array<float, 4> getGlobalCatmullRomPoint(float, std::vector<std::array<float, 3>>);
void renderCatmullRomCurve(std::vector<std::array<float, 3>>);
void renderNormals(Modelo mod);


/* Funcoes */

Textura carregaTextura(const char* srcTextura) {
	Textura res;

	unsigned int ima[1];
	ilGenImages(1, ima);
	if (ilLoadImage((ILstring)srcTextura)) {
		ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		int width = ilGetInteger(IL_IMAGE_WIDTH);
		int height = ilGetInteger(IL_IMAGE_HEIGHT);
		unsigned char *imageData = ilGetData();

		glGenTextures(1, &res.texID);
		glBindTexture(GL_TEXTURE_2D, res.texID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	return res;
}

Textura getTextura(const char* srcTextura) {
	if (!texturas.count(srcTextura)) {
		texturas[srcTextura] = carregaTextura(srcTextura);
	}
	return texturas[srcTextura];
}

Modelo leListaTriangulos(const char* srcModelo)
{
	Modelo res;
	std::vector<Triangulo> triang;
	std::vector<double> normais;
	std::vector<double> textura;

	std::fstream file;
	file.open(srcModelo, std::fstream::in);

	if (file) {

		int nTriangulos;
		file >> nTriangulos;

		double aX, aY, aZ, bX, bY, bZ, cX, cY, cZ;
		double naX, naY, naZ, nbX, nbY, nbZ, ncX, ncY, ncZ;
		double taX, taY, tbX, tbY, tcX, tcY;

		std::string linha;
		while (std::getline(file, linha)) {
			sscanf_s(linha.data(), "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf", &aX, &aY, &aZ, &bX, &bY, &bZ, &cX, &cY, &cZ, &naX, &naY, &naZ, &nbX, &nbY, &nbZ, &ncX, &ncY, &ncZ, &taX, &taY, &tbX, &tbY, &tcX, &tcY);
			Triangulo t;
			t.a.x = aX;
			t.a.y = aY;
			t.a.z = aZ;
			normais.push_back(naX);
			normais.push_back(naY);
			normais.push_back(naZ);
			textura.push_back(taX);
			textura.push_back(taY);

			t.b.x = bX;
			t.b.y = bY;
			t.b.z = bZ;
			normais.push_back(nbX);
			normais.push_back(nbY);
			normais.push_back(nbZ);
			textura.push_back(tbX);
			textura.push_back(tbY);

			t.c.x = cX;
			t.c.y = cY;
			t.c.z = cZ;
			normais.push_back(ncX);
			normais.push_back(ncY);
			normais.push_back(ncZ);
			textura.push_back(tcX);
			textura.push_back(tcY);

			triang.push_back(t);
		}

		file.close();
	}
	res.numTris = triang.size() * 3;

	glGenBuffers(3, res.buff);
	glBindBuffer(GL_ARRAY_BUFFER, res.buff[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(double)* 3 * res.numTris, triang.data(), GL_STATIC_DRAW);


	glBindBuffer(GL_ARRAY_BUFFER, res.buff[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(double)* normais.size(), normais.data(), GL_STATIC_DRAW);


	glBindBuffer(GL_ARRAY_BUFFER, res.buff[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(double)* textura.size(), textura.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return res;
}

Modelo getListaTriangulos(const char* srcModelo)
{
	// modelo ainda n�o foi carregado
	if (!modelos.count(srcModelo)) {
		modelos[srcModelo] = leListaTriangulos(srcModelo);
	}

	return modelos[srcModelo];
}

void resetBancoModelos() {
	modelos.clear();
}

void resetBancoTexturas() {
	texturas.clear();
}

void clearMaterial()
{
	float amb[] = { 0.2, 0.2, 0.2, 1.0 };
	float diff[] = { 0.8, 0.8, 0.8, 1.0 };
	float spec[] = { 0.0, 0.0, 0.0, 1.0 };
	float emit[] = { 0.0, 0.0, 0.0, 1.0 };

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emit);
}

void processaMateriais(const TiXmlElement* modelo)
{
	float diffR, diffG, diffB;
	diffR = diffG = diffB = 0.8;
	if (modelo->Attribute("diffR")) {
		diffR = atof(modelo->Attribute("diffR"));
	}
	if (modelo->Attribute("diffG")) {
		diffG = atof(modelo->Attribute("diffG"));
	}
	if (modelo->Attribute("diffB")) {
		diffB = atof(modelo->Attribute("diffB"));
	}

	float cor[4] = { diffR, diffG, diffB, 1.0f };
	glMaterialfv(GL_FRONT, GL_DIFFUSE, cor);

	float emitR, emitG, emitB;
	emitR = emitG = emitB = 0;

	if (modelo->Attribute("emitR")) {
		emitR = atof(modelo->Attribute("emitR"));
	}
	if (modelo->Attribute("emitG")) {
		emitG = atof(modelo->Attribute("emitG"));
	}
	if (modelo->Attribute("emitB")) {
		emitB = atof(modelo->Attribute("emitB"));
	}
	float emit[4] = { emitR, emitG, emitB, 1.0f };
	glMaterialfv(GL_FRONT, GL_EMISSION, emit);


	float ambR, ambG, ambB;
	ambR = ambG = ambB = 0.2;

	if (modelo->Attribute("ambR")) {
		ambR = atof(modelo->Attribute("ambR"));
	}
	if (modelo->Attribute("ambG")) {
		ambG = atof(modelo->Attribute("ambG"));
	}
	if (modelo->Attribute("ambB")) {
		ambB = atof(modelo->Attribute("ambB"));
	}
	float amb[4] = { ambR, ambG, ambB, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, amb);
}

int desenhaListaTriangulos(Modelo mod)
{
	glBindBuffer(GL_ARRAY_BUFFER, mod.buff[0]);
	glVertexPointer(3, GL_DOUBLE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, mod.buff[1]);
	glNormalPointer(GL_DOUBLE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, mod.buff[2]);
	glTexCoordPointer(2, GL_DOUBLE, 0, 0);

	glDrawArrays(GL_TRIANGLES, 0, mod.numTris);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	clearMaterial();

	if (toogle_normals){
		renderNormals(mod);
	}

	return EXIT_SUCCESS;
}

int desenhaModelo(const TiXmlNode* modelo)
{
	if (!modelo || strcmp(modelo->Value(), "modelo")) {
		return EXIT_FAILURE;
	}

	const char* nomeModelo = modelo->ToElement()->Attribute("ficheiro");

	if (!nomeModelo) {
		std::cout << "Erro no XML (<modelo> n�o cont�m tag ficheiro)\n";
		return EXIT_FAILURE;
	}

	Modelo mod = getListaTriangulos(nomeModelo);

	if (mod.numTris <= 0) {
		std::cout << "Modelo indicado n�o existe, ou cont�m erros [" << nomeModelo << "]\n";
		return EXIT_FAILURE;
	}

	const char* nomeTextura = modelo->ToElement()->Attribute("textura");
	if (nomeTextura) {
		glBindTexture(GL_TEXTURE_2D, getTextura(nomeTextura).texID);
	}

	processaMateriais(modelo->ToElement());

	int r = desenhaListaTriangulos(mod);
	glBindTexture(GL_TEXTURE_2D, 0);
	glPushAttrib(GL_COLOR_BUFFER_BIT);
	return r;
}

void aplicaAnimacaoPontos(const TiXmlNode* op)
{
	std::vector<std::array<float, 3>> pontos;
	auto trans = op->ToElement();

	float tempo = 0;

	if (trans->Attribute("tempo")) {
		tempo = atof(trans->Attribute("tempo"));

		for (auto ponto = op->FirstChild(); ponto; ponto = ponto->NextSibling()) {
			std::array<float, 3> pt = { 0, 0, 0 };
			if (!strcmp(ponto->Value(), "ponto")) {
				auto p = ponto->ToElement();

				if (p->Attribute("X")) {
					pt[0] = atof(p->Attribute("X"));
				}
				if (p->Attribute("Y")) {
					pt[1] = atof(p->Attribute("Y"));
				}
				if (p->Attribute("Z")) {
					pt[2] = atof(p->Attribute("Z"));
				}
				pontos.push_back(pt);
			}
		}

		if (pontos.size() >= 4) {

			int time = glutGet(GLUT_ELAPSED_TIME);

			float a = fmodf(time / 1000.0f, tempo) / tempo;

			//printf("%d -> a = %f\n", time, a);

			auto res = getGlobalCatmullRomPoint(a, pontos);

			if (toogle_path){
				renderCatmullRomCurve(pontos);
			}
			glTranslatef(res[0], res[1], res[2]);

		}
		else {
			std::cout << "Uma translacao tem que estar definida por pelo menos 4 pontos" << std::endl;
		}
	}
}

void aplicaTranslacao(const TiXmlElement* trans)
{
	float transX, transY, transZ, tempo;
	transX = transY = transZ = tempo = 0.0;

	if (trans->Attribute("X")) {
		transX = atof(trans->Attribute("X"));
	}

	if (trans->Attribute("Y")) {
		transY = atof(trans->Attribute("Y"));
	}

	if (trans->Attribute("Z")) {
		transZ = atof(trans->Attribute("Z"));
	}

	glTranslatef(transX, transY, transZ);

}

void aplicaEscala(const TiXmlElement* esca)
{
	float escalaX, escalaY, escalaZ;
	escalaX = escalaY = escalaZ = 1.0;

	if (esca->Attribute("X")) {
		escalaX = atof(esca->Attribute("X"));
	}

	if (esca->Attribute("Y")) {
		escalaY = atof(esca->Attribute("Y"));
	}

	if (esca->Attribute("Z")) {
		escalaZ = atof(esca->Attribute("Z"));
	}

	glScalef(escalaX, escalaY, escalaZ);
}

void aplicaRotacao(const TiXmlElement* rota)
{
	int angulo = 0;
	float eixoX, eixoY, eixoZ;
	eixoX = eixoY = eixoZ = 0.0;

	if (rota->Attribute("eixoX")) {
		eixoX = atof(rota->Attribute("eixoX"));
	}

	if (rota->Attribute("eixoY")) {
		eixoY = atof(rota->Attribute("eixoY"));
	}

	if (rota->Attribute("eixoZ")) {
		eixoZ = atof(rota->Attribute("eixoZ"));
	}

	if (rota->Attribute("angulo")) {
		angulo = atoi(rota->Attribute("angulo"));
	}
	else if (rota->Attribute("tempo")) {
		float tempo = atof(rota->Attribute("tempo"));
		int time = glutGet(GLUT_ELAPSED_TIME);
		float a = fmodf(time / 1000.0f, tempo) / tempo;
		angulo = a * 360;
	}

	glRotatef(angulo, eixoX, eixoY, eixoZ);
}

int desenhaGrupo(const TiXmlNode* grupo)
{
	if (!grupo || strcmp(grupo->Value(), "grupo")) {
		return EXIT_FAILURE;
	}

	glPushMatrix();

	int wasModelo = 0;
	int wasGrupo = 0;
	for (const TiXmlNode* op = grupo->FirstChild(); op; op = op->NextSibling()) {
		int isOp = 0;
		if (!strcmp(op->Value(), "transla��o") || !strcmp(op->Value(), "translacao")) {
			isOp = 1;
			aplicaTranslacao(op->ToElement());
			aplicaAnimacaoPontos(op);
		}
		else if (!strcmp(op->Value(), "escala")) {
			isOp = 1;
			aplicaEscala(op->ToElement());
		} //Come�a valida��o do XML
		else if (!strcmp(op->Value(), "rota��o") || !strcmp(op->Value(), "rotacao")) {
			isOp = 1;
			aplicaRotacao(op->ToElement());
		}
		else if (!strcmp(op->Value(), "modelos")) {
			if (!wasModelo) {
				wasModelo = 1;
				for (const TiXmlNode* modelo = op->FirstChild(); modelo; modelo = modelo->NextSibling()) {
					desenhaModelo(modelo);
				}
			} else {
				return EXIT_FAILURE;
			}
		}
		else if (!strcmp(op->Value(), "grupo")) {
			wasGrupo = 1;
			desenhaGrupo(op);
		}

		if (isOp && (wasModelo || wasGrupo)) {
			return EXIT_FAILURE;
		}
	}

	glPopMatrix();

	return EXIT_SUCCESS;
}

int desenhaCena(TiXmlDocument* doc)
{
	auto cena = doc->RootElement();

	TiXmlNode* grupo;
	for (grupo = cena->FirstChild("grupo"); grupo != NULL; grupo = grupo->NextSibling("grupo")) {
		if (desenhaGrupo(grupo) == EXIT_FAILURE) {
			std::cout << "XML Inv�lido (s� deve existir uma sec��o de <modelos>, e as transforma��es devem estar antes de <grupos> e <modelos>)\n";
		}
	}

	return EXIT_SUCCESS;
}

/*
=============================
Curvas
=============================
*/

std::array<float, 4> getCatmullRomPoint(float t, std::array<int, 4> indices, std::vector<std::array<float, 3>> p)
{
	int i;
	// catmull-rom matrix
	float m[4][4] = {
		{ -0.5f, 1.5f, -1.5f, 0.5f },
		{ 1.0f, -2.5f, 2.0f, -0.5f },
		{ -0.5f, 0.0f, 0.5f, 0.0f },
		{ 0.0f, 1.0f, 0.0f, 0.0f }
	};

	std::array<float, 4> res = { 0, 0, 0, 0 };

	// Calcular o ponto res = T * M * P
	// sendo Pi = p[indices[i]]

	float aux[4] = { pow(t, 3), pow(t, 2), t, 1 };

	float tm[4] = { 0, 0, 0, 0 };
	for (i = 0; i < 4; i++) {
		tm[i] = aux[0] * m[0][i]
			+ aux[1] * m[1][i]
			+ aux[2] * m[2][i]
			+ aux[3] * m[3][i];
	}

	for (i = 0; i < 3; i++) {
		res[i] = tm[0] * (p[indices[0]][i])
			+ tm[1] * (p[indices[1]][i])
			+ tm[2] * (p[indices[2]][i])
			+ tm[3] * (p[indices[3]][i]);
	}
	return res;
}

// given  global t, returns the point in the curve
std::array<float, 4> getGlobalCatmullRomPoint(float gt, std::vector<std::array<float, 3>> p)
{
	auto size = p.size();

	float t = gt * size; // this is the real global t
	int index = floor(t);  // which segment
	t = t - index; // where within  the segment

	// indices store the points
	std::array<int, 4> indices;

	indices[0] = (index + size - 1) % size;
	indices[1] = (indices[0] + 1) % size;
	indices[2] = (indices[1] + 1) % size;
	indices[3] = (indices[2] + 1) % size;

	return getCatmullRomPoint(t, indices, p);
}

void renderCatmullRomCurve(std::vector<std::array<float, 3>> pontos)
{
	float a = 0;

	// desenhar a curva usando segmentos de reta - GL_LINE_LOOP
	glDisable(GL_LIGHTING); 
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 1.0f, 1.0f);
	while (a < 1) {
		auto res = getGlobalCatmullRomPoint(a, pontos);
		glVertex3f(res[0], res[1], res[2]);
		a += 0.01;
	}
	glEnd();
	glEnable(GL_LIGHTING);
}

void renderNormals(Modelo mod)
{
	buffer_coords.reserve(mod.numTris * 3);
	buffer_norms.reserve(mod.numTris * 3);

	glBindBuffer(GL_ARRAY_BUFFER, mod.buff[0]);
	glGetBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(double)* mod.numTris * 3, buffer_coords.data());

	glBindBuffer(GL_ARRAY_BUFFER, mod.buff[1]);
	glGetBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(double)* mod.numTris * 3, buffer_norms.data());

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	{
		glColor3f(0.0f, 1.0f, 1.0f);
		for (unsigned int i = 0; i < mod.numTris * 3; i += 3) {
			glVertex3f(buffer_coords[i], buffer_coords[i + 1], buffer_coords[i + 2]);
			glVertex3f(buffer_coords[i] + buffer_norms[i] * normFac, buffer_coords[i + 1] + buffer_norms[i + 1] * normFac, buffer_coords[i + 2] + buffer_norms[i + 2] * normFac);
		}
	}
	glEnd();
	glEnable(GL_LIGHTING);
}

Lights str2lights(const char* str) 
{
	if (!str) 
		return ERR;

	if (!strcmp(str, "POINT")) {
		return POINT;
	}
	else if (!strcmp(str, "DIRECT")) {
		return DIRECT;
	}
	else if (!strcmp(str, "SPOT")) {
		return SPOT;
	}
	return ERR;
}

void colocaLuz(const TiXmlNode* l, int index)
{
	const char* tipo = "";
	float posX, posY, posZ;
	float dirX, dirY, dirZ;
	float angulo;

	posX = posY = posZ = 0;
	dirX = dirZ = 0; dirY = -1.0f;
	angulo = 30;
	auto luz = l->ToElement();

	// tipo da lampada
	if (luz->Attribute("tipo")) {
		tipo = luz->Attribute("tipo");
	}

	if (luz->Attribute("angulo")) {
		angulo = atof(luz->Attribute("angulo"));
	}

	//posicao da lampada
	if (luz->Attribute("posX")) {
		posX = atof(luz->Attribute("posX"));
	}
	if (luz->Attribute("posY")) {
		posY = atof(luz->Attribute("posY"));
	}
	if (luz->Attribute("posZ")) {
		posZ = atof(luz->Attribute("posZ"));
	}

	if (luz->Attribute("dirX")) {
		dirX = atof(luz->Attribute("dirX"));
	}
	if (luz->Attribute("dirY")) {
		dirY = atof(luz->Attribute("dirY"));
	}
	if (luz->Attribute("dirZ")) {
		dirZ = atof(luz->Attribute("dirZ"));
	}

	GLfloat pos[4] = { posX, posY, posZ, 1.0f };
	GLfloat dir[] = { dirX, dirY, dirZ, 0.0f };

	switch (str2lights(tipo)) {
	case POINT:
		glLightfv(GL_LIGHT0 + index, GL_POSITION, pos);
		break;
	case DIRECT:
		pos[3] = 0.0f;
		glLightfv(GL_LIGHT0 + index, GL_POSITION, pos);
		break;
	case SPOT:
		glLightf(GL_LIGHT0 + index, GL_SPOT_CUTOFF, angulo);
		glLightfv(GL_LIGHT0 + index, GL_SPOT_DIRECTION, dir);
		glLightf(GL_LIGHT0 + index, GL_SPOT_EXPONENT, 1);
		glLightfv(GL_LIGHT0 + index, GL_POSITION, pos);
		break;
	default:
		break;
	}
}

void colocaLuzes(TiXmlDocument* doc)
{
	auto luzes = doc->RootElement()->FirstChild("luzes");

	if (luzes != NULL) {
		int lights = 0;
		for (auto op = luzes->FirstChild("luz"); (lights < GL_MAX_LIGHTS) && op; op = op->NextSibling("luz")) {
			colocaLuz(op, lights);
			lights++;
		}
	}
}

int aplicaConfigsIniciais(TiXmlDocument* doc) {
	TiXmlElement* configs = doc->RootElement()->FirstChildElement("configuracoes");

	if (!configs) return EXIT_FAILURE;

	if (configs->Attribute("raioCamara")) {
		camRaio = atoi(configs->Attribute("raioCamara"));
	}

	if (configs->Attribute("alphaCamara")) {
		camAlpha = atoi(configs->Attribute("alphaCamara"));
	}

	if (configs->Attribute("betaCamara")) {
		camBeta = atoi(configs->Attribute("betaCamara"));
	}

	camX = camRaio * sin(camAlpha * 3.14 / 180.0) * cos(camBeta * 3.14 / 180.0);
	camZ = camRaio * cos(camAlpha * 3.14 / 180.0) * cos(camBeta * 3.14 / 180.0);
	camY = camRaio *								sin(camBeta * 3.14 / 180.0);

	return EXIT_SUCCESS;
}



void activaLuz(TiXmlNode *node, int index) 
{
	auto luz = node->ToElement();

	float diffR, diffG, diffB;
	float ambR, ambG, ambB;

	diffR = diffG = diffB = 0.0f;
	ambR = ambG = ambB = 0.0f;

	bool diffFlag = false;
	bool ambFlag = false;

	if (luz->Attribute("diffR")) {
		diffR = atof(luz->Attribute("diffR"));
		diffFlag = true;
	}
	if (luz->Attribute("diffG")) {
		diffG = atof(luz->Attribute("diffG"));
		diffFlag = true;
	}
	if (luz->Attribute("diffB")) {
		diffB = atof(luz->Attribute("diffB"));
		diffFlag = true;
	}

	if (!diffFlag) {
		diffR = diffG = diffB = 1.0f;
	}

	if (luz->Attribute("ambR")) {
		ambR = atof(luz->Attribute("ambR"));
	}
	if (luz->Attribute("ambG")) {
		ambG = atof(luz->Attribute("ambG"));
	}
	if (luz->Attribute("ambB")) {
		ambB = atof(luz->Attribute("ambB"));
	}

	GLfloat diff[] = { diffR, diffG, diffB, 1.0f };
	GLfloat amb[] = { ambR, ambG, ambB, 1.0f };
	GLfloat spec[] = { 1.0, 1.0, 1.0, 1.0f };

	glEnable(GL_LIGHT0 + index);
	glLightfv(GL_LIGHT0 + index, GL_DIFFUSE, diff);
	glLightfv(GL_LIGHT0 + index, GL_AMBIENT, amb);
	glLightfv(GL_LIGHT0 + index, GL_SPECULAR, spec);
}

void activaLuzes(TiXmlDocument* doc)
{
	auto luzes = doc->RootElement()->FirstChild("luzes");

	if (luzes) {
		int lights = 0;
		for (auto op = luzes->FirstChild("luz"); (lights < GL_MAX_LIGHTS) && op; op = op->NextSibling("luz")) {
			if (str2lights(op->ToElement()->Attribute("tipo")) != ERR) {
				activaLuz(op, lights);
				lights++;
			}
		}
	}
	else {
		glDisable(GL_LIGHTING);
	}
}

void handleKeyPress(unsigned char key, int x, int y)
{
	double passo = camRaio*0.05;
	switch (tolower(key)) {
	case 27: //Escape key
		exit(EXIT_SUCCESS);
		break;
	case 'q':
		camRaio += 1;
		break;
	case 'e':
		camRaio -= 1;
		break;
	case 'a':
		olharX -= passo;
		break;
	case 'd':
		olharX += passo;
		break;
	case 's':
		olharY -= passo;
		break;
	case 'w':
		olharY += passo;
		break;
	case 'j':
		normFac -= 0.01;
		break;
	case 'k':
		normFac += 0.01;
		break;
	case 'r':
		resetBancoModelos();
		resetBancoTexturas();
		break;
	}
	camX = camRaio * sin(camAlpha * 3.14 / 180.0) * cos(camBeta * 3.14 / 180.0);
	camZ = camRaio * cos(camAlpha * 3.14 / 180.0) * cos(camBeta * 3.14 / 180.0);
	camY = camRaio *								sin(camBeta * 3.14 / 180.0);

	glutPostRedisplay();
}

void handleArrowPress(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_LEFT:
		camAlpha -= 1;
		break;
	case GLUT_KEY_RIGHT:
		camAlpha += 1;
		break;
	case GLUT_KEY_DOWN:
		camBeta -= 1;
		break;
	case GLUT_KEY_UP:
		camBeta += 1;
		break;
	}
	if (camBeta > 85.0) {
		camBeta = 85.0;
	} else if (camBeta < -85.0) {
		camBeta = -85.0;
	}
	camX = camRaio * sin(camAlpha * 3.14 / 180.0) * cos(camBeta * 3.14 / 180.0);
	camZ = camRaio * cos(camAlpha * 3.14 / 180.0) * cos(camBeta * 3.14 / 180.0);
	camY = camRaio *								sin(camBeta * 3.14 / 180.0);

	glutPostRedisplay();
}

void changeSize(int w, int h)
{
	// Prevent a divide by zero, when window is too short
	// (you cant make a window with zero width).
	if (h == 0) {
		h = 1;
	}

	// compute window's aspect ratio
	float ratio = w * 1.0 / h;

	// Set the projection matrix as current
	glMatrixMode(GL_PROJECTION);
	// Load Identity Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set perspective
	gluPerspective(45.0f, ratio, 1.0f, 1000.0f);

	// return to the model view matrix mode
	glMatrixMode(GL_MODELVIEW);
}

void renderScene(void)
{
	// clear buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
	gluLookAt(camX, camY, camZ,
		olharX, olharY, 0.0,
		0.0, 1.0, 0.0);

	// posi��o da luz
	colocaLuzes(&cena);

	// p�r instru��es de desenho aqui
	desenhaCena(&cena);

	// End of frame
	glutSwapBuffers();
}

int verificaDoc(TiXmlDocument* doc)
{
	int luzes = 0;

	if (!doc) {
		return EXIT_FAILURE;
	}

	auto root = doc->RootElement();

	if (!root || strcmp(root->Value(), "cena")) {
		std::cout << "XML Inv�lido (n�o cont�m <cena>)\n";
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

void mode_menu(int id_op)
{
	switch (id_op) {
	case 0:
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
	case 1:
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;
	case 2:
		glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		break;
	case 3:
		glEnable(GL_CULL_FACE);
		break;
	case 4:
		glDisable(GL_CULL_FACE);
		break;
	case 5:
		toogle_path = !toogle_path;
		break;
	case 6:
		toogle_normals = !toogle_normals;
		break;
	}
	glutPostRedisplay();
}

void processMouseButtons(int button, int state, int xx, int yy)
{
	if (state == GLUT_DOWN)  {
		startX = xx;
		startY = yy;
		if (button == GLUT_LEFT_BUTTON)
			tracking = 1;
		else if (button == GLUT_MIDDLE_BUTTON)
			tracking = 2;
		else
			tracking = 0;
	}
	else if (state == GLUT_UP) {
		if (tracking == 1) {
			camAlpha += (xx - startX);
			camBeta += (yy - startY);
			if (camBeta > 85.0) {
				camBeta = 85.0;
			} else if (camBeta < -85.0) {
				camBeta = -85.0;
			}
		}
		else if (tracking == 2) {

			camRaio -= yy - startY;
			if (camRaio < 3)
				camRaio = 3.0;
		}
		tracking = 0;
	}
}


void processMouseMotion(int xx, int yy)
{
	int deltaX, deltaY;
	int alphaAux, betaAux;
	int rAux;

	if (!tracking)
		return;

	deltaX = xx - startX;
	deltaY = yy - startY;

	if (tracking == 1) {

		alphaAux = camAlpha + deltaX;
		betaAux = camBeta + deltaY;

		if (betaAux > 85.0)
			betaAux = 85.0;
		else if (betaAux < -85.0)
			betaAux = -85.0;

		rAux = camRaio;
	}
	else if (tracking == 2) {

		alphaAux = camAlpha;
		betaAux = camBeta;
		rAux = camRaio - deltaY;
		if (rAux < 3)
			rAux = 3;
	}
	camX = rAux * sin(alphaAux * 3.14 / 180.0) * cos(betaAux * 3.14 / 180.0);
	camZ = rAux * cos(alphaAux * 3.14 / 180.0) * cos(betaAux * 3.14 / 180.0);
	camY = rAux *								sin(betaAux * 3.14 / 180.0);
}


int main(int argc, char** argv)
{
	if (argc < 2) {
		std::cout << "Utiliza��o: [src.xml]\n";
		return EXIT_FAILURE;
	}

	TiXmlDocument doc(argv[1]);

	if (!doc.LoadFile()) {
		std::cout << "Erro ao ler ficheiro: " << argv[1] << "\n";
		return EXIT_FAILURE;
	}

	cena = doc;
	if (verificaDoc(&cena)){
		return EXIT_FAILURE;
	}

	// inicializa��o
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(500, 500);
	glutCreateWindow("Motor Gr�fico");

	camX = camY = camAlpha = camBeta = 0;
	camZ = camRaio = 10;

	olharX = 0;
	olharY = 0;
	normFac = 1;

	//l� defsIniciais
	aplicaConfigsIniciais(&cena);

	// registo de fun��es
	glutDisplayFunc(renderScene);
	glutIdleFunc(renderScene);
	glutReshapeFunc(changeSize);

	// p�r aqui registo da fun��es do teclado e rato
	glutKeyboardFunc(handleKeyPress);
	glutSpecialFunc(handleArrowPress);

	glutMouseFunc(processMouseButtons);
	glutMotionFunc(processMouseMotion);

	// p�r aqui a cria��o do menu
	glutCreateMenu(mode_menu);

	glutAddMenuEntry("GL_FILL", 0);
	glutAddMenuEntry("GL_LINE", 1);
	glutAddMenuEntry("GL_POINT", 2);

	glutAddMenuEntry("GL_CULL_FACE ON", 3);
	glutAddMenuEntry("GL_CULL_FACE OFF", 4);

	toogle_path = false;
	glutAddMenuEntry("TOOGLE PATHS", 5);

	toogle_path = false;
	glutAddMenuEntry("TOOGLE NORMALS", 6);

	glutAttachMenu(GLUT_RIGHT_BUTTON);

	//incializar GLEW
	glewInit();
	ilInit();
	// Ativar VBO's
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	// alguns settings para OpenGL
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// iluminacao
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	activaLuzes(&cena);

	//texturas
	glEnable(GL_TEXTURE_2D);

	// entrar no ciclo do GLUT
	glutMainLoop();

	return EXIT_SUCCESS;
}
